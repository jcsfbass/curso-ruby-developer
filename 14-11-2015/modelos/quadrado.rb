class Quadrado
	def initialize(lado)
		@lado = lado
	end

	def perimetro
		@lado * 4
	end

	def area
		@lado ** 2
	end

	def to_s
		"Quadrado: #{@lado}"
	end
end