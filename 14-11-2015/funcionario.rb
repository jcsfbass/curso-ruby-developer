class Funcionario
	attr_reader :nome, :valor_hora, :horas_trabalhadas

	def initialize(nome, valor_hora)
		@nome = nome
		@valor_hora = valor_hora
		@horas_trabalhadas = 0
	end

	def adicionar_horas(horas)
		@horas_trabalhadas += horas
	end

	def salario
		@horas_trabalhadas * @valor_hora
	end
end