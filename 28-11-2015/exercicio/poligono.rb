class Poligono
	def initialize(*lados)
		@lados = lados
	end

	def perimetro
		perimetro = 0
		@lados.each { |lado| perimetro += lado }

		perimetro
	end

	def maior_que?(poligono)
		perimetro > poligono.perimetro
	end
end