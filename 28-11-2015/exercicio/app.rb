require_relative 'poligono'
require_relative 'retangulo'
require_relative 'triangulo'

retangulo = Retangulo.new(1,2)
retangulo2 = Retangulo.new(5,7)

triangulo = Triangulo.new(1,2,3)
triangulo2 = Triangulo.new(5,7,9)

puts(retangulo.maior_que? retangulo2)
puts(retangulo2.maior_que? retangulo)

puts(triangulo.maior_que? triangulo2)
puts(triangulo2.maior_que? triangulo)