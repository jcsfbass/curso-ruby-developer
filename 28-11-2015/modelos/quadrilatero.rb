class Quadrilatero
	attr_reader :lados

	def initialize(lado1, lado2, lado3, lado4)
		@lados = [
			lado1,
			lado2,
			lado3,
			lado4
		]
	end

	def perimetro
		perimetro = 0
		@lados.each { |lado| perimetro += lado }

		perimetro
	end
end