require_relative 'quadrilatero'

class Quadrado < Quadrilatero
	def initialize(lado)
		super(lado, lado, lado, lado)
	end

	def to_s
		"Quadrado: #{@lado}"
	end
end