class Dinheiro
	attr_reader :valor

	def initialize(valor)
		@valor = valor
	end

	def igual?(dinheiro)
		@valor == dinheiro.valor
	end

	def somar(*dinheiros)
		valor = @valor
		for dinheiro in dinheiros
			valor += dinheiro.valor
		end

		Dinheiro.new(valor)
	end
end

dez = Dinheiro.new(10)
outro_dez = Dinheiro.new(10)
vinte = Dinheiro.new(20)

puts dez.igual? outro_dez
puts dez.igual? vinte
puts dez.somar(outro_dez, vinte).valor