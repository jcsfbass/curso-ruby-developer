json.array!(@livros) do |livro|
  json.extract! livro, :id, :titulo, :autor, :descricao
  json.url livro_url(livro, format: :json)
end
