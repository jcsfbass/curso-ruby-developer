module Mensuravel
	attr_reader :medida

	def maior_que?(mensuravel)
		@medida > mensuravel.medida
	end
end