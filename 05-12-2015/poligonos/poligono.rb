module Poligono
	class Base
		include Mensuravel

		def initialize(*lados)
			lados.each { |lado|	raise ArgumentError, 'Argumento deve ser do tipo Fixnum' unless lado.is_a? Fixnum }
			@lados = lados
			@medida = perimetro
		end

		def perimetro
			perimetro = 0
			@lados.each { |lado| perimetro += lado }
			perimetro
		end
	end
end