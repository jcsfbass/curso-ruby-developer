require_relative 'mensuravel'
require_relative 'poligonos/poligono'
require_relative 'poligonos/retangulo'
require_relative 'poligonos/triangulo'
require_relative 'cookie'
require_relative 'validador'

# retanguloA = Poligono::Retangulo.new(1,2)
# retanguloB = Poligono::Retangulo.new(5,7)

# puts retanguloA.maior_que?(retanguloB)
# puts retanguloB.maior_que?(retanguloA)

# trianguloA = Poligono::Triangulo.new(1,2,3)
# trianguloB = Poligono::Triangulo.new(5,6,7)

# puts trianguloA.maior_que?(trianguloB)
# puts trianguloB.maior_que?(trianguloA)

# cookieA = Cookie.new(5)
# cookieB = Cookie.new(7)

# puts cookieA.maior_que?(cookieB)
# puts cookieB.maior_que?(cookieA)

primeiro_lado = '1'

begin
	p Poligono::Retangulo.new(primeiro_lado,2)
rescue ArgumentError => error
	puts 'Deu erro...'
	puts error.message
	primeiro_lado = primeiro_lado.to_i
	retry
ensure
	puts 'Vai ser executado...'
end