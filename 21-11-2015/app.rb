require_relative 'modelos/quadrado'

quadrados = [
	Quadrado.new(1),
	Quadrado.new(2),
	Quadrado.new(3)
]

dobro = quadrados.map do |quadrado|
	Quadrado.new(quadrado.lado * 2)
end

filtrado = quadrados.select do |quadrado|
	quadrado.lado > 1
end

quadrado = quadrados.find { |quadrado| quadrado.lado == 3 }

puts quadrado

metodo = lambda do |num1, num2|
	num1 + num2
end

puts metodo.call(1,2)