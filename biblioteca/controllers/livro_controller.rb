require_relative '../repositories/livro_repository'
require_relative '../factories/livro_factory'

get '/' do
	@livros = LivroRepository.all

	erb :home
end

get '/livro/novo' do
	erb :novo
end

post '/livro/criar' do
	LivroRepository.add(
		LivroFactory.create(params)
	)

	'Livro cadastrado'
end

post '/livro/deletar/:id' do |id|
	LivroRepository.delete(id)

	'Livro deletado'
end