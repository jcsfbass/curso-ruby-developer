class LivroFactory
	def self.create(params)
		Livro.new(
			titulo: params['nome'],
			descricao: params['descricao'],
			autor: params['autor'],
			quantidade: params['quantidade'].to_i
		)
	end
end