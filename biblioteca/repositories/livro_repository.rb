require_relative '../models/livro'

class LivroRepository
	@livros = [
		Livro.new(
			titulo: 'Pedra Filosofal',
			descricao: 'É bonzinho',
			autor: 'Não sei',
			quantidade: 10
		),
		Livro.new(
			autor: 'Não faço a menor ideia',
			descricao: 'É uma [...]',
			titulo: 'Cálice de Fogo',
			quantidade: 2
		)
	]

	def self.all
		@livros
	end

	def self.add(livro)
		@livros << livro
	end

	def self.delete(id)
		@livros.delete @livros.find { |livro| livro.id == id }
	end
end