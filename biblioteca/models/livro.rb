require 'securerandom'

class Livro
	attr_reader :id
	attr_reader :titulo
	attr_reader :descricao
	attr_reader :autor
	attr_reader :quantidade

	def initialize(titulo:, descricao:, autor:, quantidade:)
		@id = SecureRandom.uuid
		@titulo = titulo
		@descricao = descricao
		@autor = autor
		@quantidade = quantidade
	end

	def eql?(livro)
		@id == livro.id
	end
end